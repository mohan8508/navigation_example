import App from './App';
import Home from './src/home';
import SideBar from './src/sidebar';
import SideCenter from './src/sidecenter';
import newScreen from './src/newScreen';


import { Navigation } from "react-native-navigation";

export function registerScreens() {
    Navigation.registerComponentWithRedux("navigation.index", () => App);
    Navigation.registerComponentWithRedux("navigation.home", () => Home);
    Navigation.registerComponentWithRedux("navigation.sidebar", () => SideBar);
    Navigation.registerComponentWithRedux("navigation.sidecenter", () => SideCenter);
    Navigation.registerComponentWithRedux("navigation.newscreen", () => newScreen);
}