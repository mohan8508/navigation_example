/** @format */

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import { Navigation } from "react-native-navigation";
import { registerScreens } from './registerscreens'

registerScreens();
Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            sideMenu: {
                left: {
                    component: {
                        name: "navigation.sidebar"
                    },
                },
                center: {
                        stack: {
                            id: 'myId',
                            children: [{
                                component: {
                                    id: 'home',
                                    name: 'navigation.home',
                                    passProps: {
                                        text: 'This is tab 1'
                                    }
                                },
                                component: {
                                    id: 'newwww',
                                    name: 'navigation.sidecenter',
                                    passProps: {
                                        text: 'This is tab 1'
                                    }
                                },
                                component: {
                                    id: 'newwwwwww',
                                    name: 'navigation.newscreen',
                                    passProps: {
                                        text: 'This is tab 1'
                                    }
                                },
                                component: {
                                    id: 'newwwwwwwwwwwww',
                                    name: 'navigation.index',
                                    passProps: {
                                        text: 'This is tab 1'
                                    }
                                }
                            }],
                            options: {
                                bottomTab: {
                                    text: 'Tab 1',
                                    testID: 'FIRST_TAB_BAR_BUTTON'
                                }
                            }
                        }
                }
            },
           
        }
    });

});
