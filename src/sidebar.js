/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import { Navigation } from "react-native-navigation";

export default class SideBar extends Component {
    constructor(props) {
        super(props);
        console.log('propsss', props)
    }
    navigate = () => {
        console.log('this.props.componentId', this.props.componentId)
        console.log('Navigation', Navigation.push)
        Navigation.mergeOptions('myId', {
            sideMenu: {
              left: {
                visible: false,
              },
            },
          });
        Navigation.push("myId", {
            component: {
                name: 'navigation.home',

            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.navigate}>
                    <Text>GO FROM SIDEMENU</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
