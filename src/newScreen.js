/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button,TouchableOpacity } from 'react-native';
import { Navigation } from "react-native-navigation";

export default class newscreen extends Component {
    constructor(props) {
        super(props);
            console.log('propsss',props)
    }
    navigate = () => {
        console.log('this.props4')
        Navigation.mergeOptions(this.props.componentId, {
          component: {
            name: 'navigation.sidecenter',
            passProps: {
              text: 'Pushed screen'
            }
          }
        });
      }
    
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.navigate}>
                    <Text>NEWWW</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
