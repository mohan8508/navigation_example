/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button,TouchableOpacity } from 'react-native';
import { Navigation } from "react-native-navigation";

export default class SideCenter extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);

    }
    navigate = () => {
        console.log('this.props.componentId',this.props.componentId)
        console.log('Navigation',Navigation.push)
    
        Navigation.push(this.props.componentId, {
          component: {
            name: 'navigation.home',
            passProps: {
              text: 'Pushed screen'
            },
            options: {
              topBar: {
                title: {
                  text: 'Pushed screen title'
                }
              }
            }
          }
        });
      }
    
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.navigate}>
                    <Text>Home</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
